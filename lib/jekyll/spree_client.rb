# frozen_string_literal: true

require 'fast_blank'
require 'spree_client'

# 
module Jekyll
  SpreeClient = Struct.new :site, :sku_field, :api_key, :spree_url, :shipping_category_id, :store, keyword_init: true do
    # Products are posts with the SKU field
    #
    # @return [Array] Jekyll::Document
    def local_products
      @local_products ||= site.documents.select do |doc|
        !doc.data.dig(sku_field).to_s.blank?
      end
    end

    # SKU identifies a unique product
    #
    # @return [String]
    def sku_field
      self[:sku_field] ||= 'sku'
    end

    # Default shipping category ID
    #
    # @return [Integer]
    def shipping_category_id
      self[:shipping_category_id] ||= 1
    end

    # Fields that can change on Spree and we need to sync locally
    #
    # @return [Array]
    def variant_fields
      @variant_fields ||= %w[sku price weight height width depth cost_price].freeze
    end

    # Fields that can change locally and we need to sync to Spree
    #
    # @return [Array]
    def product_fields
      @product_fields ||= %w[name description meta_description meta_keywords meta_title pay_what_you_can extra_attributes].freeze
    end

    # All fields, using during creation
    #
    # @return [Array]
    def create_fields
      @create_fields ||= (product_fields + variant_fields).freeze
    end

    # Fields which are only updated on the Variant.
    def variant_only_fields
      @variant_only_fields ||= %w[track_inventory].freeze
    end

    # Spree Client
    #
    # @return [SpreeClient::API::V1]
    def spree
      @spree ||= ::SpreeClient::API::V1.new **to_h.slice(:api_key, :spree_url, :store)
    end

    # Localization with jekyll-locales
    # @return [Hash]
    def i18n
      @i18n ||= locale ? site.data[locale] : {}
    end

    # Current locale
    # @return [String,Nil]
    def locale
      @locale ||= site.config['locale']
    end

    def sync!
      Jekyll::Hooks.trigger :spree, :pre_render, self

      # The API allows to query several SKUs at the same time, so we send
      # groups of 10 products.
      local_products.each_slice(10) do |products|
        skus = products.map { |p| p.data[sku_field] }

        # Gather variants by their SKU
        unless response = (variants skus)
          Jekyll.logger.error "Couldn't obtain variants"

          products.each do |p|
            mark_with_error p,
              i18n.dig('spree', 'errors', 'api') || "Couldn't obtain variants, the store may be down or API key is incorrect"
          end

          next
        end

        products.each do |product|
          # Products have names, not titles, so we save them for later
          product.data['_name'] = product.data['name'] if product.data.key? 'name'
          product.data['name'] = product.data['title']

          # Remove previous errors
          product.data.delete 'errors'
          # Find the corresponding Spree variant in the response
          variant = response['variants'].find do |v|
            v[sku_field] == product.data[sku_field]
          end

          # If the variant already exists on Spree, update.
          if variant
            update product, variant
          else
            create product
          end

          product.data['name'] = product.data.delete('_name') if product.data.key? '_name'
        end
      end

      Jekyll::Hooks.trigger :spree, :post_render, self

      local_products.each do |product|
        local_save product
      end
    end

    private

    # Tries to save the product if jekyll-write-and-commit-changes is
    # enabled.  Notifies once.
    #
    # @return [true,false]
    def local_save(product)
      if product.respond_to? :save
        product.save
      else
        unless @suggest_save
          @suggest_save = true
          Jekyll.logger.info "Install jekyll-write-and-commit-changes to save local changes to products"
        end

        false
      end
    end

    # Manage products on Spree.
    #
    # XXX: We're guessing the shipping category because there's no API
    # for it.
    def remote_products
      @remote_products ||= spree.products(shipping_category_id: shipping_category_id)
    end

    # Obtain the default stock location, the first that's active.
    #
    # XXX: API doesn't show which one's the default so we're guessing.
    #
    # @return [Hash]
    def stock_location
      @stock_location ||=
        if spree.stock_locations.index
          spree.stock_locations.response['stock_locations']&.find { |s| s['active'] }
        else
          Jekyll.logger.warn "Couldn't get default stock location"
          nil
        end
    end

    # Fetch variants by SKU
    #
    # @param [Array]
    # @return [Hash]
    def variants(skus)
      spree.variants.response if spree.variants.index(q: { sku_in: skus })
    end

    def mark_with_error(product, messages = [])
      product.data['errors'] = { 'time' => Time.now, 'messages' => messages }
    end

    # Update the product and try to save changes
    #
    # @param [Jekyll::Document]
    # @param [Hash]
    # @return [true,false]
    def update(product, variant)
      # Sync local changes to Spree
      remote_product = attributes_from(product, product_fields).tap do |a|
        a[:extra_attributes] ||= {}
      end

      variant_comparison = attributes_from(variant, product_fields).tap do |v|
        v[:extra_attributes] = v[:extra_attributes]&.transform_keys(&:to_sym)
      end

      # Only when they haven't changed, so we don't waste time sending a
      # request.
      unless compare_hashes(remote_product, variant_comparison)
        # XXX: We don't have the product ID with the variant but we have
        # the slug.
        remote_variant = { id: variant['id'] }.merge!(attributes_from product, variant_only_fields)

        remote_product.delete_if do |k, v|
          remote_variant.key? k
        end

        remote_product[:id] = variant['slug']

        unless remote_products.update(**remote_product) && spree.variants.update(**remote_variant)
          Jekyll.logger.error "Couldn't update #{product.data['title']}"
          mark_with_error product, error_messages
        end
      end

      # XXX: The API replies with stringified numbers so we convert them
      # back to the type we're using locally.
      variant.slice(*variant_fields).each do |k, v|
        product.data[k] = case product.data[k]
                            when Integer then v.to_i
                            when Float then v.to_f
                            else v
                          end
      end

      product.data['in_stock'] = variant['in_stock']
      product.data['variant_id'] = variant['id']

      product
    end

    # If the product doesn't exist, create it.
    #
    # @param [Jekyll::Document]
    # @return [true,false]
    def create(product)
      new_product = attributes_from(product, create_fields)

      unless remote_products.create(**new_product)
        Jekyll.logger.error "Couldn't create #{product.data['title']}"
        mark_with_error product, error_messages

        return false
      end

      variant_id = remote_products.response.dig('master', 'id')
      spree.variants(id: variant_id).update(attributes_from(product, variant_only_fields))
      product.data['variant_id'] = variant_id
      product.data['in_stock'] = product.data['stock'].to_i > 0

      # Add the initial stock
      spree.stock_items(id: variant_id, stock_location_id: stock_location['id'])
           .stock_movements.create(quantity: product.data['stock'])

      true
    end

    # Stores errors so users can fix them
    def error_messages
      remote_products.response['errors']&.map do |field, errors|
        errors.map do |error|
          field + ' ' + error
        end
      end&.flatten
    end

    # Converts fields from a Jekyll::Document into an attribute Hash.
    #
    # @param [Jekyll::Document,Hash] Document or Hash to extract data from
    # @param [Array] An Array of field names
    # @return [Hash] A symbolized Hash of fields and values
    def attributes_from(document_or_hash, fields)
      (document_or_hash.respond_to?(:data) ? document_or_hash.data : document_or_hash).slice(*fields).transform_keys(&:to_sym)
    end

    # Compare two hashes by the keys they share.  Some fields we send
    # are not available through the APIv1, like meta_description.
    #
    # @link https://stackoverflow.com/a/15503329
    # @param :h1 [Hash]
    # @param :h2 [Hash]
    # @return [Boolean]
    def compare_hashes(h1, h2)
      (h1.keys & h2.keys).all? do |k|
        h1[k] == h2[k]
      end
    end
  end
end
