# frozen_string_literal: true

require_relative 'jekyll/spree_client'

# We'll only work with Jekyll::Documents with a SKU field in their front
# matter.  The SKU needs to be unique, but this plugin doesn't ensure
# that.  Duplicated SKUs will be silently ignored for now.
Jekyll::Hooks.register :site, :post_read do |site|
  # Ordered articles
  posts = site.site_payload['site']['posts']

  # Make steps available to other articles
  %w[cart shipment payment confirmation].each do |step|
    site.config[step] = posts.find do |doc|
      doc.data['layout'] == step
    end
  end

  if ENV['SPREE_API_KEY'].nil? || ENV['SPREE_API_KEY'].empty?
    Jekyll.logger.warn "SPREE_API_KEY environment variable missing, skipping syncrhonization"
    next
  end

  config = site.config['spree']&.transform_keys(&:to_sym) || {}
  config.merge!(site: site, api_key: ENV['SPREE_API_KEY'], spree_url: ENV['SPREE_URL'], store: site.config['url'])

  client = Jekyll::SpreeClient.new **config

  client.sync!

  if ENV['JEKYLL_ENV'] == 'production' && site.respond_to?(:repository)
    site.repository.commit site.data.dig(site.config['locale'], 'spree', 'commit') || 'Spree synchronization'
  end
end
